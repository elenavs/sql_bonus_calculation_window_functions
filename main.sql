-- Компания решила выплатить премии трем самым успешным сотрудникам (по объему продаж) в 2007 году. --
-- Выводим список всех сотрудников отдела продаж, ранг в продажах, сумму проданных товаров, заработную плату и величину премии. --

SELECT
    first_name,
    last_name,
    row_number () OVER (ORDER BY order_sum2007 DESC) rank,
    order_sum2007,
    salary,
        CASE
        WHEN order_sum2007 >= 115855.2 THEN order_sum2007/1000
        ELSE 0
        END AS bonus
FROM (
        SELECT
        DISTINCT sales_rep_id,
        dt,
        MAX (summa) OVER (PARTITION BY sales_rep_id) order_sum2007
        FROM (
                SELECT
                EXTRACT (YEAR FROM order_date) dt,
                sales_rep_id,
                SUM (order_total) OVER (PARTITION BY sales_rep_id ORDER BY order_date) summa
                FROM oe.orders 
                WHERE EXTRACT (YEAR FROM order_date) = 2007 AND
                sales_rep_id IS NOT NULL)) t1
INNER JOIN hr.employees t2
ON t1.sales_rep_id = t2.employee_id
ORDER BY rank;
